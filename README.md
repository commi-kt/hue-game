# Hue Game

[See on Heroku](https://kt4joy.herokuapp.com/)

Shared Run Configurations:
- run - runs server with Gradle continuous build and Webpack hot swap;
- stop - stops Webpack Server.

## Useful resources

- Core: 
  - [Kotlin Frontend Plugin](https://github.com/Kotlin/kotlin-frontend-plugin)
  - [Ant Design](https://ant.design/)
  - [Kotlin Ant Design Wrappers](https://github.com/samgarasx/kotlin-js-wrappers)
- Articles:
  - [Getting Started With Kotlin-React](https://medium.com/@ralf.stuckert/getting-started-with-kotlin-react-c5f3b079a8bf)
  - [Getting Started With Kotlin-React - Part II](https://medium.com/@ralf.stuckert/getting-started-with-kotlin-react-part-ii-9dda64c9b0c8)
  - [Kotlin JS wrappers (samgarasx's project for antd)](https://www.reddit.com/r/Kotlin/comments/c12z4p/kotlin_js_wrappers/)
- Stack Overflow:
  - [How to import node module in React-Kotlin?](https://stackoverflow.com/questions/51431848/how-to-import-node-module-in-react-kotlin)
- Heroku Deployment:
  - [Deploy your Webpack apps to Heroku in 3 simple steps](https://codeburst.io/deploy-your-webpack-apps-to-heroku-in-3-simple-steps-4ae072af93a8)
