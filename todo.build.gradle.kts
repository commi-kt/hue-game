//
group = "tk.labyrinth"
version = "0.1.0-SNAPSHOT"
//
buildscript {
	//
	val kotlinVersion = "1.3.60"
	//
	repositories {
		maven("https://dl.bintray.com/kotlin/kotlin-eap/")
		mavenCentral()
	}
	//
	dependencies {
		//
		// From: repositories: maven("https://dl.bintray.com/kotlin/kotlin-eap/")
		// https://github.com/Kotlin/kotlin-frontend-plugin
		//		classpath("org.jetbrains.kotlin:kotlin-frontend-plugin:0.0.45")
		classpath(kotlin("frontend-plugin", version = "0.0.45"))
		//
		// From: repositories: mavenCentral()
		// https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-gradle-plugin
		//		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
		classpath(kotlin("gradle-plugin", version = kotlinVersion))
	}
}
//
// From: buildscript: dependencies: classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version")
apply(plugin = "kotlin2js")
//
// From: buildscript: dependencies: classpath "org.jetbrains.kotlin:kotlin-frontend-plugin:0.0.45"
apply(plugin = "org.jetbrains.kotlin.frontend")
//
//
repositories {
	//
	// Required for transitive dependencies of: org.jetbrains:kotlin-react-dom
	// Required for transitive dependencies of: org.jetbrains:kotlin-react-redux
	// Required for transitive dependencies of: org.jetbrains:kotlin-styled
	jcenter()
	//
	// Required for: com.github.samgarasx:kotlin-antd
	maven("https://dl.bintray.com/samgarasx/kotlin-js-wrappers/")
	//
	// Required for: org.jetbrains:kotlin-react-dom
	// Required for: org.jetbrains:kotlin-react-redux
	// Required for: org.jetbrains:kotlin-styled
	maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	//
	// Required for: org.jetbrains.kotlin:kotlin-compiler-embeddable
	// It is used by compileKotlin2Js task.
	mavenCentral()
}
//
dependencies {
	val kotlinVersion = "1.3.60"
	val kotlinWrappersVersion = "pre.89-kotlin-$kotlinVersion"
	//
	// From: repositories: maven("https://dl.bintray.com/samgarasx/kotlin-js-wrappers/")
	// https://bintray.com/samgarasx/kotlin-js-wrappers/kotlin-antd
	"compile"("com.github.samgarasx:kotlin-antd:3.20.3-pre.0-kotlin-1.3.50")
	//
	// From: repositories: mavenCentral()
	"compile"("org.jetbrains.kotlin:kotlin-stdlib-js:$kotlinVersion")
	//
	//From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react-dom
	"compile"("org.jetbrains:kotlin-react-dom:16.9.0-$kotlinWrappersVersion")
	//
	// From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-react-redux
	"compile"("org.jetbrains:kotlin-react-redux:5.0.7-$kotlinWrappersVersion")
	//
	// From: repositories: maven("https://kotlin.bintray.com/kotlin-js-wrappers")
	// https://bintray.com/kotlin/kotlin-js-wrappers/kotlin-styled
	"compile"("org.jetbrains:kotlin-styled:1.0.0-$kotlinWrappersVersion")
	//
	"testCompile"("org.jetbrains.kotlin:kotlin-test-js:$kotlinVersion")
}
//
configure<org.jetbrains.kotlin.gradle.frontend.KotlinFrontendExtension> {
	downloadNodeJsVersion = "latest"
	//
	configure<org.jetbrains.kotlin.gradle.frontend.npm.NpmExtension> {
		//
		// Required for: com.github.samgarasx:kotlin-antd
		dependency("antd")
		//
		// Required for: org.jetbrains:kotlin-extensions
		dependency("core-js")
		//
		// Required for: org.jetbrains:kotlin-styled
		dependency("inline-style-prefixer")
		//
		// Required for: tk.labyrinth.hue.component.wrapper.ColorPickerComponent
		dependency("rc-color-picker")
		//
		// Required for: org.jetbrains:kotlin-react
		dependency("react")
		//
		// Required for: org.jetbrains:kotlin-react-dom
		dependency("react-dom")
		//
		// Required for: org.jetbrains:kotlin-react-redux
		dependency("react-redux")
		//
		// Required for: org.jetbrains:kotlin-redux
		dependency("redux")
		//
		// Required for: org.jetbrains:kotlin-styled
		dependency("styled-components")
	}
	// FIXME: Find a way to make webpackBundle work.
	bundle<org.jetbrains.kotlin.gradle.frontend.webpack.WebPackExtension>("webpack") {
		configure<org.jetbrains.kotlin.gradle.frontend.webpack.WebPackExtension> {
			bundleName = "main"
			contentPath = file("build/resources/main/web")
		}
	}
}
//
tasks {
	"compileKotlin2Js"(org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile::class) {
		kotlinOptions.main = "call"
		kotlinOptions.metaInfo = true
		kotlinOptions.moduleKind = "commonjs"
		kotlinOptions.outputFile = "${project.buildDir.path}/js/${project.name}.js"
		kotlinOptions.sourceMap = true
	}
	"compileTestKotlin2Js"(org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile::class) {
		kotlinOptions.main = "call"
		kotlinOptions.metaInfo = true
		kotlinOptions.moduleKind = "commonjs"
		kotlinOptions.outputFile = "$project.buildDir.path/js-tests/${project.name}-tests.js"
		kotlinOptions.sourceMap = true
	}
}
