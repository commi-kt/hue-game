//
// This file ensures webpack.config.d directory exists as it is required by :webpack-config task.
// See: https://github.com/Kotlin/kotlin-frontend-plugin/issues/141
