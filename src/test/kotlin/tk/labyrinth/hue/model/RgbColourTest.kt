package tk.labyrinth.hue.model

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

private class RgbColourTest {
	@Test
	fun testGetColourComponent() {
		val colour = RgbColour("#123456")
		assertEquals(0x12, colour.getRed())
		assertEquals(0x34, colour.getGreen())
		assertEquals(0x56, colour.getBlue())
	}

	@Test
	fun testInit() {
		//		assertEquals("#abc", RgbColour("#abc").toSharp())
		//		assertFailsWith<IllegalArgumentException> { RgbColour("abc") }
	}

	@Test
	fun testResolveHex() {
		assertEquals(0, resolveHex("#000"))
		assertEquals(0, resolveHex("#000000"))
		assertEquals(0xAAAAAA, resolveHex("#aaa"))
		assertEquals(0xAAAAAA, resolveHex("#aaaaaa"))
		assertEquals(0xAAAABB, resolveHex("#aab"))
		assertEquals(0xAAAAAB, resolveHex("#aaaaab"))
		assertEquals(0xFFFFFF, resolveHex("#fff"))
		assertEquals(0xFFFFFF, resolveHex("#ffffff"))
		//
		assertFails { resolveHex("") }.let {
			assertEquals(IllegalArgumentException::class, it::class)
			assertEquals("Require startsWith '#': ", it.message)
		}
		assertFails { resolveHex("abc") }.let {
			assertEquals(IllegalArgumentException::class, it::class)
			assertEquals("Require startsWith '#': abc", it.message)
		}
		assertFails { resolveHex("#") }.let {
			assertEquals(IllegalArgumentException::class, it::class)
			assertEquals("Require length of 4 or 7: #", it.message)
		}
		assertFails { resolveHex("#abababc") }.let {
			assertEquals(IllegalArgumentException::class, it::class)
			assertEquals("Require length of 4 or 7: #abababc", it.message)
		}
		assertFails { resolveHex("#ffg") }.let {
			assertEquals(NumberFormatException::class, it::class)
			assertEquals("Invalid number format: 'ffffgg'", it.message)
		}
	}

	@Test
	fun testResolveRgb() {
		assertEquals(0, resolveRgb(0, 0, 0))
		assertEquals(0x123456, resolveRgb(0x12, 0x34, 0x56))
		assertEquals(0xFFFFFF, resolveRgb(0xFF, 0xFF, 0xFF))
		//
		assertFails { resolveRgb(0, -1, 0) }.let {
			assertEquals(IllegalArgumentException::class, it::class)
			assertEquals("Require within range [0, 255]: green: -1", it.message)
		}
		assertFails { resolveRgb(0, 0x100, 0) }.let {
			assertEquals(IllegalArgumentException::class, it::class)
			assertEquals("Require within range [0, 255]: green: 256", it.message)
		}
	}
}
