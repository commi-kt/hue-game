package tk.labyrinth.hue

import kotlin.test.Test
import kotlin.test.assertEquals

private class DummyTest {
	/**
	 * This test is used to check if testing framework is working as expected.
	 */
	@Test
	fun dummyTest() {
		assertEquals("a", "a")
		assertEquals(1, 1)
	}
}
