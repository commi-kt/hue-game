package tk.labyrinth.hue.model

data class GradientCell(
	val colour: RgbColour,
	val columnIndex: ColumnIndex,
	val movable: Boolean,
	val rowIndex: RowIndex
)
