package tk.labyrinth.hue.model

data class Point(
	val x: Double,
	val y: Double
)
