package tk.labyrinth.hue.model

import tk.labyrinth.hue.validation.Validations

private const val hexRadix = 16
private const val oneByteOffset = 8
private const val twoByteOffset = 16

data class RgbColour(
	val rgb: Int
) {
	init {
		Validations.requireWithinRange(rgb, 0, 0xFFFFFF, toInclusive = true)
	}

	constructor(hex: String) : this(resolveHex(hex))
	constructor(red: Int, green: Int, blue: Int) : this(resolveRgb(red, green, blue))

	fun getBlue(): Int {
		return rgb and 0xFF
	}

	fun getGreen(): Int {
		return rgb shr oneByteOffset and 0xFF
	}

	fun getRed(): Int {
		return rgb shr twoByteOffset and 0xFF
	}

	fun toHex(): String {
		return "#${rgb.toString(hexRadix).toUpperCase().padStart(6, '0')}"
	}
}

internal fun resolveHex(hex: String): Int {
	if (!hex.startsWith("#")) {
		throw IllegalArgumentException("Require startsWith '#': $hex")
	}
	val subhex = hex.substring(1)
	return when (hex.length) {
		4 /* #FFF */ -> subhex.fold("", { first, second -> first + second + second })
		7 /* #FFFFFF */ -> subhex
		else -> throw IllegalArgumentException("Require length of 4 or 7: $hex")
	}.toInt(hexRadix)
}

internal fun resolveRgb(red: Int, green: Int, blue: Int): Int {
	return (Validations.requireByte(red, "red") shl twoByteOffset) +
		(Validations.requireByte(green, "green") shl oneByteOffset) +
		Validations.requireByte(blue, "blue")
}
