package tk.labyrinth.hue.model.ornament

fun cornerRule(): Ornament {
	return Ornament("Corners") { dimension, cell ->
		cell.rowIndex.value.let { it == 0 || it == dimension.rowCount.value - 1 } and
			cell.columnIndex.value.let { it == 0 || it == dimension.columnCount.value - 1 }
	}
}

fun borderRule(): Ornament {
	return Ornament("Border") { dimension, cell ->
		cell.rowIndex.value.let { it == 0 || it == dimension.rowCount.value - 1 } or
			cell.columnIndex.value.let { it == 0 || it == dimension.columnCount.value - 1 }
	}
}
