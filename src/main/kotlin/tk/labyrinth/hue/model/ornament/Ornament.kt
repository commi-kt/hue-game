package tk.labyrinth.hue.model.ornament

import tk.labyrinth.hue.model.Grid

data class Ornament(
	val name: String,
	/**
	 * True -> cell is fixed, false - movable.
	 */
	val predicate: (Grid.Dimension, Grid.Cell) -> Boolean
)
