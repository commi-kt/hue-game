package tk.labyrinth.hue.model

data class Grid(
	val cards: List<List<Card>>
) {
	fun cardAt(cell: Cell): Card {
		return cards[cell.rowIndex.value][cell.columnIndex.value]
	}

	fun columnCount(): ColumnCount {
		return ColumnCount(cards[0].size)
	}

	fun dimension(): Dimension {
		return Dimension(cards.size, cards[0].size)
	}

	fun forEach(function: (Dimension, Card) -> Unit) {
		val dimension = dimension()
		cards.forEach { row -> row.forEach { card -> function(dimension, card) } }
	}

	fun map(function: (Dimension, Card) -> Card): Grid {
		val dimension = dimension()
		return Grid(cards.map { row -> row.map { card -> function(dimension, card) } })
	}

	fun rowCount(): RowCount {
		return RowCount(cards.size)
	}

	data class Card(
		val cell: Cell,
		val colour: RgbColour,
		val movable: Boolean
	)

	data class Cell(
		val columnIndex: ColumnIndex,
		val rowIndex: RowIndex
	)

	data class Dimension(
		val columnCount: ColumnCount,
		val rowCount: RowCount
	) {
		constructor(rowCount: Int, columnCount: Int) : this(ColumnCount(columnCount), RowCount(rowCount))

		fun <T> map(mapFunction: (Cell) -> T): List<List<T>> {
			return rowCount.toList { rowIndex -> columnCount.toList { columnIndex -> mapFunction(Cell(columnIndex, rowIndex)) } }
		}
	}
}

data class ColumnCount(val value: Int) {
	fun <T> toList(function: (ColumnIndex) -> T): List<T> {
		return IntRange(0, value - 1).map { ColumnIndex(it) }.map(function)
	}
}

data class ColumnIndex(
	val value: Int
)

data class GridDimension(
	val columnCount: ColumnCount,
	val rowCount: RowCount
) {
	constructor(rowCount: Int, columnCount: Int) : this(ColumnCount(columnCount), RowCount(rowCount))

	fun <T> map(mapFunction: (RowIndex, ColumnIndex) -> T): List<List<T>> {
		return rowCount.toList { rowIndex -> columnCount.toList { columnIndex -> mapFunction(rowIndex, columnIndex) } }
	}
}

//
data class RowCount(val value: Int) {
	fun <T> toList(function: (RowIndex) -> T): List<T> {
		return IntRange(0, value - 1).map { RowIndex(it) }.map(function)
	}
}

//
data class RowIndex(val value: Int)
