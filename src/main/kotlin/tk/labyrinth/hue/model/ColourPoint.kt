package tk.labyrinth.hue.model

data class ColourPoint(
	val colour: RgbColour,
	val point: Point
) {
	enum class Position(val areaName: String, val point: Point) {
		EAST("e", Point(1.0, 0.5)),
		NORTH("n", Point(0.5, 0.0)),
		NORTH_EAST("ne", Point(1.0, 0.0)),
		NORTH_WEST("nw", Point(0.0, 0.0)),
		SOUTH("s", Point(0.5, 1.0)),
		SOUTH_EAST("se", Point(1.0, 1.0)),
		SOUTH_WEST("sw", Point(0.0, 1.0)),
		WEST("w", Point(0.0, 0.5))
	}
}
