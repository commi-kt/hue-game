package tk.labyrinth.hue.util

import tk.labyrinth.hue.model.ColourPoint
import tk.labyrinth.hue.model.Grid
import tk.labyrinth.hue.model.Point
import tk.labyrinth.hue.model.RgbColour
import tk.labyrinth.hue.model.ornament.Ornament
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlin.random.Random

object GridUtils {
	//
	private fun calcColourUnscaledWeight(dimension: Grid.Dimension, cell: Grid.Cell, point: Point): Double {
		return 1 / sqrt(((point.x * dimension.columnCount.value) - (cell.columnIndex.value + 0.5)).pow(2)
			.plus(((point.y * dimension.rowCount.value) - (cell.rowIndex.value + 0.5)).pow(2)))
	}

	fun createGrid(dimension: Grid.Dimension, colourPoints: List<ColourPoint>, ornament: Ornament): Grid {
		return Grid(dimension.map { cell ->
			val colourUnscaledWeightMap = colourPoints.associate {
				it.colour to calcColourUnscaledWeight(dimension, cell, it.point)
			}
			val weightSum = colourUnscaledWeightMap.values.sum()
			val colourScaledWeightMap = colourUnscaledWeightMap.mapValues {
				when {
					weightSum.isFinite() -> it.value / weightSum
					// TODO: There may be multiple infinite points (in practice they not, but we want to cover this case anyway)
					it.value.isInfinite() -> 1.0
					else -> 0.0
				}
			}
			Grid.Card(
				cell,
				colourScaledWeightMap.entries.fold(RgbColour("#000")) { first, second ->
					RgbColour(first.getRed() + (second.key.getRed() * second.value).roundToInt(),
						first.getGreen() + (second.key.getGreen() * second.value).roundToInt(),
						first.getBlue() + (second.key.getBlue() * second.value).roundToInt())
				},
				!ornament.predicate(dimension, cell)
			)
		})
	}

	fun resetGrid(grid: Grid): Grid {
		val cellCardMap = grid.cards.flatten().associateBy { it.cell }
		return Grid(grid.dimension().map { cell -> cellCardMap.getValue(cell) })
	}

	fun shuffleGrid(grid: Grid): Grid {
		val remainingCards = grid.cards.flatten().filter { it.movable }.toMutableList()
		return Grid(grid.dimension().map { cell ->
			val card = grid.cardAt(cell)
			if (card.movable) remainingCards.removeAt(Random.nextInt(remainingCards.size))
			else card
		})
	}
}
