package tk.labyrinth.hue.state

import tk.labyrinth.hue.model.ColourPoint
import tk.labyrinth.hue.model.Grid
import tk.labyrinth.hue.model.RgbColour
import tk.labyrinth.hue.model.ornament.Ornament
import tk.labyrinth.hue.model.ornament.borderRule
import tk.labyrinth.hue.util.GridUtils

data class CanvasGameModel(
	val grid: Grid
)

data class CanvasModel(
	val game: CanvasGameModel,
	val setup: CanvasSetupModel,
	val stage: CanvasStage
) {
	constructor(setup: CanvasSetupModel) : this(
		game = CanvasGameModel(GridUtils.createGrid(setup.dimension, setup.colourPoints, setup.ornament)),
		setup = setup,
		stage = CanvasStage.SETUP
	)
}

data class CanvasParentModel(
	val canvas: CanvasModel = CanvasModel(
		setup = CanvasSetupModel(
			colourPoints = listOf(
				ColourPoint(RgbColour("#0F0"), ColourPoint.Position.NORTH_WEST.point),
				ColourPoint(RgbColour("#0FF"), ColourPoint.Position.NORTH_EAST.point),
				ColourPoint(RgbColour("#FF0"), ColourPoint.Position.SOUTH_WEST.point),
				ColourPoint(RgbColour("#00F"), ColourPoint.Position.SOUTH_EAST.point)
			),
			dimension = Grid.Dimension(7, 7),
			ornament = borderRule()
		)
	)
)

data class CanvasSetupModel(
	val colourPoints: List<ColourPoint>,
	val dimension: Grid.Dimension,
	val ornament: Ornament
)

enum class CanvasStage(val index: Int) {
	GAME(1),
	SETUP(0)
}

data class GlobalState(
	val canvasParent: CanvasParentModel = CanvasParentModel()
)
