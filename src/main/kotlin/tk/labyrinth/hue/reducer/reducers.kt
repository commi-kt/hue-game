package tk.labyrinth.hue.reducer

import redux.RAction
import redux.Reducer
import tk.labyrinth.hue.action.ChangeCanvasSetupAction
import tk.labyrinth.hue.action.ChangeCanvasStageAction
import tk.labyrinth.hue.action.RandomizeCardsAction
import tk.labyrinth.hue.state.CanvasParentModel
import tk.labyrinth.hue.state.CanvasStage
import tk.labyrinth.hue.state.GlobalState
import tk.labyrinth.hue.util.GridUtils
import kotlin.reflect.KProperty1

fun canvasParent(state: CanvasParentModel = CanvasParentModel(), action: RAction): CanvasParentModel = when (action) {
	is ChangeCanvasSetupAction -> {
		val setup = action.value
		state.copy(
			canvas = state.canvas.let { canvas ->
				canvas.copy(
					game = if (canvas.stage == CanvasStage.SETUP)
						canvas.game.copy(
							grid = GridUtils.createGrid(setup.dimension, setup.colourPoints, setup.ornament)
						)
					else canvas.game,
					setup = setup
				)
			}
		)
	}
	is ChangeCanvasStageAction -> {
		val stage = action.value
		state.copy(
			canvas = state.canvas.let { canvas ->
				canvas.copy(
					game = canvas.game.let { game ->
						if (stage == CanvasStage.SETUP)
							game.copy(
								grid = GridUtils.resetGrid(game.grid)
							)
						else game
					},
					stage = stage
				)
			}
		)
	}
	is RandomizeCardsAction -> {
		state.copy(
			canvas = state.canvas.let { canvas ->
				canvas.copy(
					game = canvas.game.let { game ->
						game.copy(
							grid = GridUtils.shuffleGrid(game.grid)
						)
					}
				)
			}
		)
	}
	else -> {
		state
	}
}

fun combinedReducers() = combineReducers(
	mapOf(
		GlobalState::canvasParent to ::canvasParent
	)
)

fun <S, A, R> combineReducers(reducers: Map<KProperty1<S, R>, Reducer<*, A>>): Reducer<S, A> {
	return redux.combineReducers(reducers.mapKeys { it.key.name })
}
