package tk.labyrinth.hue

import org.w3c.dom.Document
import react.dom.render
import react.redux.provider
import redux.RAction
import redux.compose
import redux.createStore
import redux.rEnhancer
import tk.labyrinth.hue.component.application
import tk.labyrinth.hue.reducer.combinedReducers
import tk.labyrinth.hue.state.GlobalState
import tk.labyrinth.hue.style.applyGlobalStyles
import kotlin.browser.document

fun init(document: Document) {
	document.getElementById("root")?.let { root ->
		applyGlobalStyles()
		render(root) { provider(store) { application {} } }
	}
}

fun main() {
	if (document.body != null) {
		init(document)
	} else {
		document.addEventListener("DOMContentLoaded", {
			init(document)
		})
	}
}

val store = createStore<GlobalState, RAction, dynamic>(
	combinedReducers(),
	GlobalState(),
	// FIXME: Find out what does code below do.
	compose(
		rEnhancer(),
		js("if(window.__REDUX_DEVTOOLS_EXTENSION__ )window.__REDUX_DEVTOOLS_EXTENSION__ ();else(function(f){return f;});")
	)
)
