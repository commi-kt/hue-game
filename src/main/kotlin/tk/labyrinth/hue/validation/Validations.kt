package tk.labyrinth.hue.validation

object Validations {
	fun requireByte(value: Int, name: String? = null): Int {
		return requireWithinRange(value, 0, 255, toInclusive = true, name = name)
	}

	fun requireWithinRange(value: Int, from: Int, to: Int, fromInclusive: Boolean = true, toInclusive: Boolean = false, name: String? = null): Int {
		val fromViolation = if (fromInclusive) value < from else value <= from
		val toViolation = if (toInclusive) value > to else value >= to
		if (fromViolation || toViolation) {
			val range = "${if (fromInclusive) "[" else "("}$from, $to${if (toInclusive) "]" else ")"}"
			val nameBlock = if (name != null) "$name:" else ""
			throw IllegalArgumentException("Require within range $range: $nameBlock $value")
		}
		return value
	}
}
