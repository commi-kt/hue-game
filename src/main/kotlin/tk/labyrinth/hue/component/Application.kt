package tk.labyrinth.hue.component

import antd.form.form
import antd.form.formItem
import kotlinx.css.Display
import kotlinx.css.FlexDirection
import kotlinx.css.JustifyContent
import kotlinx.css.display
import kotlinx.css.flexDirection
import kotlinx.css.height
import kotlinx.css.justifyContent
import kotlinx.css.pct
import kotlinx.css.width
import react.RClass
import react.RProps
import react.dom.a
import react.invoke
import react.redux.rConnect
import redux.RAction
import redux.WrapperAction
import styled.css
import styled.styledDiv
import styled.styledFooter
import styled.styledH1
import styled.styledHeader
import tk.labyrinth.hue.action.ChangeCanvasSetupAction
import tk.labyrinth.hue.action.ChangeCanvasStageAction
import tk.labyrinth.hue.action.RandomizeCardsAction
import tk.labyrinth.hue.state.CanvasSetupModel
import tk.labyrinth.hue.state.CanvasStage
import tk.labyrinth.hue.state.GlobalState
import tk.labyrinth.hue.style.Layout

private class Component : react.RComponent<Component.Props, react.RState>() {
	override fun react.RBuilder.render() {
		styledHeader {
			css {
				+Layout.horizontal
				justifyContent = JustifyContent.center
			}
			//
			styledH1 {
				+"Hue Game"
			}
		}
		styledDiv {
			css {
				display = Display.flex
				flexDirection = FlexDirection.row
				height = 100.pct
				width = 100.pct
			}
			canvasSettings {
				attrs.model = CanvasSettings.Model(props.model.canvasParent.canvas.setup, props.model.canvasParent.canvas.stage)
				attrs.model = props.model.canvasParent.canvas.let { canvas -> CanvasSettings.Model(canvas.setup, canvas.stage) }
				attrs.onCardsRandomize = props.onCardsRandomize
				attrs.onSetupChange = props.onCanvasSetupChange
				attrs.onStageChange = props.onCanvasStageChange
			}
			canvas {
				attrs.model = props.model.canvasParent.canvas
				attrs.onSetupChange = props.onCanvasSetupChange
			}
		}
		styledFooter {
			css {
				+Layout.horizontal
				justifyContent = JustifyContent.center
			}
			//
			form {
				attrs.layout = "inline"
				//
				formItem {
					attrs.label = "Project"
					//
					a(href = "https://gitlab.com/commi-kt/hue-game", target = "_blank") {
						+"Hue Game"
					}
				}
				formItem {
					attrs.label = "Inspired by"
					//
					a(href = "https://play.google.com/store/apps/details?id=com.color.puzzle.i.love.hue.blendoku.game", target = "_blank") {
						+"Color Puzzle Game"
					}
				}
			}
		}
	}

	interface Props : RProps {
		var model: GlobalState
		var onCanvasSetupChange: (CanvasSetupModel) -> Unit
		var onCanvasStageChange: (CanvasStage) -> Unit
		var onCardsRandomize: () -> Unit
	}
}

val application: RClass<RProps> =
	rConnect<GlobalState, RAction, WrapperAction, RProps, Component.Props, Component.Props, RProps>(
		{ state, _ ->
			model = state
		},
		{ dispatch, _ ->
			onCanvasSetupChange = { value -> dispatch(ChangeCanvasSetupAction(value)) }
			onCanvasStageChange = { value -> dispatch(ChangeCanvasStageAction(value)) }
			onCardsRandomize = { dispatch(RandomizeCardsAction()) }
		}
	)(Component::class.js.unsafeCast<RClass<RProps>>())
