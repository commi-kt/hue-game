package tk.labyrinth.hue.component

import kotlinx.css.Display
import kotlinx.css.FlexDirection
import kotlinx.css.display
import kotlinx.css.flexDirection
import kotlinx.html.DIV
import react.RBuilder
import styled.StyledDOMBuilder
import styled.css
import styled.styledDiv

inline fun RBuilder.hbox(block: StyledDOMBuilder<DIV>.() -> Unit) {
	styledDiv {
		css {
			block.also {
				display = Display.flex
				flexDirection = FlexDirection.row
			}
		}
	}
}
