package tk.labyrinth.hue.component

import antd.input.input
import antd.inputnumber.inputNumber
import kotlinx.css.Align
import kotlinx.css.Display
import kotlinx.css.FlexDirection
import kotlinx.css.LinearDimension
import kotlinx.css.alignItems
import kotlinx.css.display
import kotlinx.css.flexDirection
import kotlinx.css.height
import kotlinx.css.minHeight
import kotlinx.css.minWidth
import kotlinx.css.pct
import kotlinx.css.px
import kotlinx.css.rem
import kotlinx.css.width
import react.RBuilder
import react.RComponent
import react.RHandler
import react.RProps
import react.RState
import styled.css
import styled.styledDiv
import tk.labyrinth.hue.component.wrapper.rccolorpicker.colorPicker
import tk.labyrinth.hue.model.ColourPoint
import tk.labyrinth.hue.model.Point
import tk.labyrinth.hue.model.RgbColour

object ColourPointBox {
	internal class Component : RComponent<Props, RState>() {
		override fun RBuilder.render() {
			styledDiv {
				css {
					alignItems = Align.stretch
					descendants(".colour-input") {
						width = LinearDimension.auto
					}
					descendants(".colour-input .ant-input-group-addon") {
						//					width = 3.rem
					}
					descendants(".colour-input .ant-input") {
						width = 5.rem
					}
					descendants(".colour-picker .rc-color-picker-trigger") {
						height = 100.pct
						minHeight = 20.px
						minWidth = 20.px
						width = 100.pct
					}
					display = Display.flex
					flexDirection = FlexDirection.row
				}
				input {
					attrs.addonBefore = "#"
					attrs.className = "colour-input"
					attrs.disabled = !props.enabled
					attrs.onChange = { event: dynamic -> props.onColourChange?.invoke(RgbColour(event.target.value as String)) }
					attrs.placeholder = "Colour"
					attrs.value = props.value.colour.toHex()
				}
				colorPicker {
					attrs.className = "colour-picker"
					attrs.color = props.value.colour.toHex()
					attrs.enableAlpha = false
					attrs.onChange = if (props.enabled) { state -> props.onColourChange?.invoke(RgbColour(state.color)) } else null
				}
				inputNumber {
					attrs.disabled = !props.enabled
					//					attrs.onChange = { value: Int -> props.onPointChange?.invoke(value) }
					attrs.placeholder = "Index"
					attrs.step = 5
					//					attrs.value = props.value.point
				}
			}
		}
	}

	interface Props : RProps {
		var enabled: Boolean
		var onColourChange: ((RgbColour) -> Unit)?
		var onPointChange: ((Point) -> Unit)?
		var value: ColourPoint
	}
}

fun RBuilder.colourPointBox(handler: RHandler<ColourPointBox.Props>) = child(ColourPointBox.Component::class, handler)
