package tk.labyrinth.hue.component

import react.RBuilder
import react.RComponent
import react.RHandler
import react.RProps
import react.RState
import tk.labyrinth.hue.component.wrapper.antd.ListComponent
import tk.labyrinth.hue.component.wrapper.antd.list
import tk.labyrinth.hue.model.ColourPoint
import tk.labyrinth.misc4kt.collectoin.ListUtils

/**
 * https://css-houdini.rocks/corners-gradient
 */
object ColourPointList {
	class Component : RComponent<Props, RState>() {
		override fun RBuilder.render() {
			list<ColourPoint, ListComponent<ColourPoint>> {
				attrs.dataSource = props.value.toTypedArray()
				attrs.renderItem = { item, index ->
					colourPointBox {
						attrs.enabled = props.enabled
						attrs.value = item
						attrs.onColourChange = { value ->
							props.onChange(ListUtils.replaceElement(props.value, index.toInt())
							{ element -> element.copy(colour = value) })
						}
						attrs.onPointChange = { value ->
							props.onChange(ListUtils.replaceElement(props.value, index.toInt())
							{ element -> element.copy(point = value) })
						}
					}
				}
			}
		}
	}

	interface Props : RProps {
		var enabled: Boolean
		var onChange: (List<ColourPoint>) -> Unit
		var value: List<ColourPoint>
	}
}

fun RBuilder.colourPointList(handler: RHandler<ColourPointList.Props>) = child(ColourPointList.Component::class, handler)
