package tk.labyrinth.hue.component

import kotlinx.css.Align
import kotlinx.css.Display
import kotlinx.css.FlexDirection
import kotlinx.css.JustifyContent
import kotlinx.css.alignItems
import kotlinx.css.display
import kotlinx.css.flexDirection
import kotlinx.css.flexGrow
import kotlinx.css.height
import kotlinx.css.justifyContent
import kotlinx.css.pct
import kotlinx.css.width
import react.RBuilder
import react.RComponent
import react.RHandler
import react.RProps
import react.RState
import styled.css
import styled.styledDiv
import tk.labyrinth.hue.component.canvas.canvasBoard
import tk.labyrinth.hue.component.canvas.canvasSetupLayer
import tk.labyrinth.hue.state.CanvasModel
import tk.labyrinth.hue.state.CanvasSetupModel
import tk.labyrinth.hue.state.CanvasStage
import tk.labyrinth.hue.style.Layout

object Canvas2 {
	class Component : RComponent<Props, RState>() {
		override fun RBuilder.render() {
			styledDiv {
				css {
					alignItems = Align.center
					display = Display.flex
					flexDirection = FlexDirection.row
					flexGrow = 1.0
					justifyContent = JustifyContent.center
				}
				styledDiv {
					css {
						+Layout.stack
						height = 80.pct
						width = 80.pct
					}
					canvasBoard {
						attrs.model = props.model.game
					}
					if (props.model.stage == CanvasStage.SETUP) canvasSetupLayer {
						attrs.model = props.model.setup
						attrs.onChange = props.onSetupChange
					}
				}
			}
		}
	}

	interface Props : RProps {
		var model: CanvasModel
		var onSetupChange: (CanvasSetupModel) -> Unit
	}
}

fun RBuilder.canvas(handler: RHandler<Canvas2.Props>) =
	child(Canvas2.Component::class, handler)
