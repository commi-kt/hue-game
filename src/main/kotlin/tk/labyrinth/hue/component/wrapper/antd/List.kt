@file:JsModule("antd/lib/list")

package tk.labyrinth.hue.component.wrapper.antd

import antd.list.ListProps
import antd.list.ListState
import react.Component

/**
 * Typesafe variant of [antd.list.ListComponent].
 */
@JsName("default")
external class ListComponent<T> : Component<ListProps<T>, ListState> {
	override fun render(): dynamic
}
