package tk.labyrinth.hue.component.wrapper.rccolorpicker

import react.Component
import react.RProps
import react.RState

/**
 * Based on antd.inputnumber.InputNumberComponent.<br>
 * https://github.com/react-component/color-picker<br>
 */
@JsModule("rc-color-picker")
external object ColorPickerComponent : Component<ColorPickerProps, ColorPickerState> {
	override fun render(): dynamic
}

external interface ColorPickerProps : RProps {
	var className: String
	var color: String
	var defaultColor: String
	var enableAlpha: Boolean
	var onChange: ((ColorPickerState) -> Unit)?
	var placement: String
}

external interface ColorPickerState : RState {
	var alpha: Boolean
	var color: String
}
