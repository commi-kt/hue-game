package tk.labyrinth.hue.component.wrapper.rccolorpicker

import react.RBuilder
import react.RHandler

fun RBuilder.colorPicker(handler: RHandler<ColorPickerProps>) = child(ColorPickerComponent::class, handler)
