package tk.labyrinth.hue.component.wrapper.antd

import antd.list.ListProps
import react.RBuilder
import react.RHandler

/**
 * Typesafe variant of [antd.list.list].
 */
inline fun <T, reified L : ListComponent<T>> RBuilder.list(noinline handler: RHandler<ListProps<T>>) = child(L::class, handler)
