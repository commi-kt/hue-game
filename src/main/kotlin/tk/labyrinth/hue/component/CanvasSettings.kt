package tk.labyrinth.hue.component

import antd.button.button
import antd.button.buttonGroup
import antd.card.card
import antd.form.form
import antd.form.formItem
import antd.inputnumber.inputNumber
import kotlinext.js.jsObject
import react.RBuilder
import react.RComponent
import react.RHandler
import react.RProps
import react.RState
import styled.css
import styled.styledDiv
import tk.labyrinth.hue.model.ColumnCount
import tk.labyrinth.hue.model.RowCount
import tk.labyrinth.hue.state.CanvasSetupModel
import tk.labyrinth.hue.state.CanvasStage
import tk.labyrinth.hue.style.Layout

object CanvasSettings {
	class Component : RComponent<Props, RState>() {
		override fun RBuilder.render() {
			styledDiv {
				css {
					+Layout.vertical
				}
				//
				card {
					attrs.title = "Grid"
					//
					form {
						// TODO: Make it span-independent.
						attrs.layout = "horizontal"
						attrs.labelCol = jsObject {
							span = 10
						}
						attrs.wrapperCol = jsObject {
							// Value here changes nothing, it is required to make labelCol work.
							span = 1
						}
						//
						formItem {
							attrs.label = "Rows"
							//
							inputNumber {
								attrs.disabled = props.model.stage != CanvasStage.SETUP
								attrs.min = 5
								attrs.onChange = { value: Int ->
									props.onSetupChange(props.model.setup.let { setup ->
										setup.copy(dimension = setup.dimension.copy(rowCount = RowCount(value)))
									})
								}
								attrs.value = props.model.setup.dimension.rowCount.value
							}
						}
						formItem {
							attrs.label = "Columns"
							//
							inputNumber {
								attrs.disabled = props.model.stage != CanvasStage.SETUP
								attrs.min = 5
								attrs.onChange = { value: Int ->
									props.onSetupChange(props.model.setup.let { setup ->
										setup.copy(dimension = setup.dimension.copy(columnCount = ColumnCount(value)))
									})
								}
								attrs.value = props.model.setup.dimension.columnCount.value
							}
						}
					}
				}
				buttonGroup {
					button {
						attrs.block = true
						attrs.disabled = props.model.stage == CanvasStage.SETUP
						attrs.onClick = { props.onStageChange(CanvasStage.SETUP) }
						//
						+"Setup"
					}
					button {
						attrs.block = true
						attrs.disabled = props.model.stage == CanvasStage.GAME
						attrs.onClick = { props.onStageChange(CanvasStage.GAME) }
						//
						+"Game"
					}
				}
				button {
					attrs.block = true
					attrs.disabled = props.model.stage != CanvasStage.GAME
					attrs.onClick = { props.onCardsRandomize() }
					//
					+"Randomize Cards"
				}
			}
		}
	}

	interface Props : RProps {
		var model: Model
		var onCardsRandomize: () -> Unit
		var onSetupChange: (CanvasSetupModel) -> Unit
		var onStageChange: (CanvasStage) -> Unit
	}

	data class Model(
		val setup: CanvasSetupModel,
		val stage: CanvasStage
	)
}

fun RBuilder.canvasSettings(handler: RHandler<CanvasSettings.Props>) = child(CanvasSettings.Component::class, handler)
