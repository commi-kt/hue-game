package tk.labyrinth.hue.component.canvas

import kotlinx.css.Display
import kotlinx.css.GridTemplateAreas
import kotlinx.css.JustifyContent
import kotlinx.css.display
import kotlinx.css.gridTemplateAreas
import kotlinx.css.justifyContent
import react.RBuilder
import react.RComponent
import react.RHandler
import react.RProps
import react.RState
import styled.css
import styled.styledDiv
import tk.labyrinth.hue.component.wrapper.rccolorpicker.colorPicker
import tk.labyrinth.hue.model.ColourPoint
import tk.labyrinth.hue.model.RgbColour
import tk.labyrinth.hue.state.CanvasSetupModel
import tk.labyrinth.misc4kt.collectoin.ListUtils

object CanvasSetupLayer {
	class Component : RComponent<Props, RState>() {
		override fun RBuilder.render() {
			styledDiv {
				css {
					put("align-content", "space-between")
					display = Display.grid
					gridTemplateAreas = GridTemplateAreas(
						"\"nw n ne\" " +
							"\"w . e\n\" " +
							"\"sw s se\""
					)
					justifyContent = JustifyContent.spaceBetween
				}
				//
				val positions = listOf(ColourPoint.Position.NORTH_WEST, ColourPoint.Position.NORTH_EAST,
					ColourPoint.Position.SOUTH_WEST, ColourPoint.Position.SOUTH_EAST)
				props.model.colourPoints.forEachIndexed { index, colourPoint ->
					styledDiv {
						css.put("grid-area", positions[index].areaName)
						//
						colorPicker {
							attrs.className = "colour-picker"
							attrs.color = colourPoint.colour.toHex()
							attrs.enableAlpha = false
							attrs.onChange = { state ->
								props.onChange(props.model.let { model ->
									model.copy(colourPoints = ListUtils.replaceElement(model.colourPoints, index) { colourPoint ->
										colourPoint.copy(colour = RgbColour(state.color))
									})
								})
							}
						}
					}
				}
			}
		}
	}

	interface Props : RProps {
		var model: CanvasSetupModel
		var onChange: (CanvasSetupModel) -> Unit
	}
}

fun RBuilder.canvasSetupLayer(handler: RHandler<CanvasSetupLayer.Props>) = child(CanvasSetupLayer.Component::class, handler)
