package tk.labyrinth.hue.component.canvas

import kotlinx.css.Align
import kotlinx.css.Color
import kotlinx.css.Display
import kotlinx.css.GridTemplateColumns
import kotlinx.css.alignSelf
import kotlinx.css.backgroundColor
import kotlinx.css.display
import kotlinx.css.gridTemplateColumns
import kotlinx.css.height
import kotlinx.css.px
import kotlinx.css.width
import react.RBuilder
import react.RComponent
import react.RHandler
import react.RProps
import react.RState
import styled.css
import styled.inlineStyles
import styled.styledDiv
import tk.labyrinth.hue.state.CanvasGameModel
import tk.labyrinth.hue.style.Layout

object CanvasBoard {
	class Component : RComponent<Props, RState>() {
		override fun RBuilder.render() {
			styledDiv {
				val grid = props.model.grid
				//
				css.display = Display.grid
				css.gridTemplateColumns = GridTemplateColumns.repeat("${grid.columnCount().value}, 1fr")
				//
				grid.forEach { _, card ->
					styledDiv {
						css {
							+Layout.stack
						}
						inlineStyles {
							backgroundColor = Color(card.colour.toHex())
						}
						//
						styledDiv {
							+"${card.cell.rowIndex.value}-${card.cell.columnIndex.value}"
						}
						if (!card.movable) styledDiv {
							css {
								alignSelf = Align.center
								height = 20.px
								put("justify-self", "center")
								width = 20.px
							}
							//
							+"X"
						}
					}
				}
			}
		}
	}

	interface Props : RProps {
		var model: CanvasGameModel
	}
}

fun RBuilder.canvasBoard(handler: RHandler<CanvasBoard.Props>) = child(CanvasBoard.Component::class, handler)
