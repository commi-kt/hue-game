package tk.labyrinth.hue.action

import redux.RAction
import tk.labyrinth.hue.state.CanvasSetupModel
import tk.labyrinth.hue.state.CanvasStage

//
class ChangeCanvasStageAction(val value: CanvasStage) : RAction

//// Setup
//
class ChangeCanvasSetupAction(val value: CanvasSetupModel) : RAction

//// Game
//
class RandomizeCardsAction : RAction
