package tk.labyrinth.hue.style

import kotlinx.css.BoxSizing
import kotlinx.css.body
import kotlinx.css.boxSizing
import kotlinx.css.height
import kotlinx.css.html
import kotlinx.css.margin
import kotlinx.css.pct
import kotlinx.css.px
import styled.StyleSheet
import styled.StyledComponents
import styled.injectGlobal

fun applyGlobalStyles() {
	StyledComponents.injectGlobal {
		// TODO: Check if antd provides this feature.
		universal {
			boxSizing = BoxSizing.borderBox
		}
		descendants("#root") {
			+Layout.vertical
			height = 100.pct
		}
	}
}

/**
 * Do not use it now as antd css provides same features.
 */
object Global : StyleSheet("global") {
	val default by css {
		body {
			height = 100.pct
			margin(0.px)
		}
		html {
			height = 100.pct
		}
	}
}
