package tk.labyrinth.hue.style

import kotlinx.css.Display
import kotlinx.css.FlexDirection
import kotlinx.css.GridTemplateAreas
import kotlinx.css.display
import kotlinx.css.flexDirection
import kotlinx.css.gridTemplateAreas
import styled.StyleSheet

object Layout : StyleSheet("layout") {
	val horizontal by css {
		display = Display.flex
		flexDirection = FlexDirection.row
	}
	val stack by css {
		val centre = "centre"
		//
		children {
			put("grid-area", centre)
		}
		display = Display.grid
		gridTemplateAreas = GridTemplateAreas("\"$centre\"")
	}
	val vertical by css {
		display = Display.flex
		flexDirection = FlexDirection.column
	}
}
