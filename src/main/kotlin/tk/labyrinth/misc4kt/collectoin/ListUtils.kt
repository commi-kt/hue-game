package tk.labyrinth.misc4kt.collectoin

object ListUtils {
	fun <E> replaceElement(list: List<E>, index: Int, function: (E) -> E): List<E> {
		val mutableList = list.toMutableList()
		mutableList[index] = function.invoke(mutableList[index])
		return mutableList.toList()
	}
}
